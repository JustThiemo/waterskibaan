﻿
using System.Collections.Generic;
using System.Timers;
using System.Windows;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using Waterskibaan;
using System.Windows.Shapes;

namespace WaterskibaanGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private Game game = new Game();

        public MainWindow()
        {
            InitializeComponent();
            game.Initialize();

            Timer timer = new Timer(1000);
            timer.Elapsed += game.OnTimePassed;
            timer.Elapsed += ScreenTick;
            timer.Start();
        }

        private void ScreenTick(object sender, ElapsedEventArgs eventArgs)
        {
            this.Dispatcher.Invoke(() =>
            {
                wachtrijInstructieAantal.Text = $"{game.wachtrijInstructie.Queue.Count}";
                instructieGroepAantal.Text = $"{game.instructieGroep.Queue.Count}";
                wachtrijStartenAantal.Text = $"{game.wachtrijStarten.Queue.Count}";
                lijnenAantal.Text = $"{game.skibaan._voorraad.GetAantalLijnen()}";
                bezoekersAantal.Text = $"{game.logger._bezoekers.Count}";
                aantalRondes.Text = $"{game.logger.GetTotaleRondes()}";
                aantalChappies.Text = $"{game.logger.BezoekersInRood}";
                scoreAantal.Text = $"{game.logger.GetHighScore()}";

                TekenWachtrij(game.wachtrijInstructie.Queue, canvasWachtrij);
                TekenWachtrij(game.instructieGroep.Queue, canvasInstructie);
                TekenWachtrij(game.wachtrijStarten.Queue, canvasStarten);

                canvasWater.Children.Clear();

                canvasWater.Children.Add(canvasMoves);
                TekenMoves();

                KabelTekenen();
                PompLichteKleurenNeer();
            });
        }

        public void RegistreerMove(object sender, SporterEventArgs eventArgs)
        {
            Lijn lijn = eventArgs.Sporter.OpLijn;
            if(lijn != null)
            {

                int x = 350, y = 500;
                int i = lijn.PositieOpDeKabel + 1;
                this.Dispatcher.Invoke(() =>
                {
                    PlaatsSporter(x, (i >= 5 && i != 10 ? y + 100 : y - 100), lijn);
                });
            }
        }

        public void TekenMoves()
        {
            allMoves.Items.Clear();
            // Add to fancy list box :D
            game.logger.UniekeMoves(game?.skibaan?._kabel?._lijnen).ForEach(naam => allMoves.Items.Add(naam));
        }

        public void TekenWachtrij(Queue<Sporter> wachtrij, Canvas canvas)
        {
            int x = 0;
            int y = 5;

            canvas.Children.Clear();

            for (int i = 1; i < wachtrij.Count + 1; i++)
            {
                {
                    Ellipse circle = new Ellipse();
                    System.Drawing.Color kleding = wachtrij.ElementAt(i - 1).KledingKleur;
                    Color convertedKleur = Color.FromArgb(kleding.A, kleding.R, kleding.G, kleding.B);

                    {
                        circle.Fill = new SolidColorBrush(convertedKleur);
                        circle.Width = 20;
                        circle.Height = 20;
                        circle.Stroke = new SolidColorBrush(Colors.Black);
                    }

                    Canvas.SetLeft(circle, x);
                    Canvas.SetTop(circle, y);
                    canvas.Children.Add(circle);

                    if (x >= 530)
                    {
                        y += 40;
                        x = 0;
                    }
                    else x += 30;

                }

            }
        }
        public void KabelTekenen()
        {

            int x = 350, y = 500;

            for (int i = 1; i <= 10; i++)
            {
                Label positie = new Label();

                SolidColorBrush brush = new SolidColorBrush();
                brush.Color = Colors.Black;
                positie.Content = i;

                x = (i <= 5 || i == 10 ? x + 80 : x - 80);
                if (i == 5)
                {
                    x -= 80;
                    y += 80;
                }

                if (i == 10)
                {
                    y -= 80;
                    x -= 80;
                }

                Lijn lijn = GetLijnVanPositie(i - 1);
                if (lijn != null)
                    PlaatsSporter(x, (i >= 5 && i != 10 ? y + 35 : y - 30), lijn);

                Canvas.SetLeft(positie, x);
                Canvas.SetTop(positie, y);
                canvasWater.Children.Add(positie);
            }
        }

        private Lijn GetLijnVanPositie(int l)
        {
            foreach (Lijn lijn in game.skibaan._kabel._lijnen)
            {
                if (lijn.PositieOpDeKabel == l) return lijn;
            }

            return null;
        }

        public void PompLichteKleurenNeer()
        {

            canvasKleur.Children.Clear();
            int x = 0, y = 5;

            foreach (Sporter sp in game.logger.GetLichsteKleuren())
            {

                Ellipse circle = new Ellipse();
                System.Drawing.Color kleding = sp.KledingKleur;
                Color convertedKleur = Color.FromArgb(kleding.A, kleding.R, kleding.G, kleding.B);

                {
                    circle.Fill = new SolidColorBrush(convertedKleur);
                    circle.Width = 20;
                    circle.Height = 20;
                    circle.Stroke = new SolidColorBrush(Colors.Black);
                }

                Canvas.SetLeft(circle, x);
                Canvas.SetTop(circle, y);

                canvasKleur.Children.Add(circle);
                x += 30;
            }
        }


        public void PlaatsSporter(int x, int y, Lijn l)
        {
            Ellipse circle = new Ellipse();
            System.Drawing.Color kleding = l.Sporter.KledingKleur;
            Color convertedKleur = Color.FromArgb(kleding.A, kleding.R, kleding.G, kleding.B);

            {
                circle.Fill = new SolidColorBrush(convertedKleur);
                circle.Width = 20;
                circle.Height = 20;
                circle.Stroke = new SolidColorBrush(Colors.Black);
            }

            Canvas.SetLeft(circle, x);
            Canvas.SetTop(circle, y);
            canvasWater.Children.Add(circle);

            Sporter sporter = l.Sporter;
            if(sporter.DoMove())
            {

                TextBlock blonkie = new TextBlock();
                blonkie.Text = sporter.HuidigeMove.Naam;
                if(l.PositieOpDeKabel <= 3 || l.PositieOpDeKabel == 9)
                {
                    Canvas.SetTop(blonkie, y + 50);
                } else
                {
                    Canvas.SetTop(blonkie, y - 50);
                }
                Canvas.SetLeft(blonkie, x);
             
                canvasWater.Children.Add(blonkie);
            }
        }


    }
}
