﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Waterskibaan;
using Waterskibaan.Wachtrij;

namespace UnitTestWaterskibaan
{
    [TestClass]
    public class SimpleUnitTest
    {

        [TestMethod]
        public void Test_Opdract2()
        {
            LijnenVoorraad voorraad = new LijnenVoorraad();
            Assert.IsNotNull(voorraad, "Voorraad was null");

            Lijn lijn = new Lijn();
            Assert.AreEqual(lijn.PositieOpDeKabel, 0, "Start positie lijn is niet 0");
            voorraad.LijnToevoegenAanRij(lijn);

            Assert.AreEqual(voorraad.GetAantalLijnen(), 1, "Voorraad is niet toegevoegd");
            voorraad.VerwijderEersteLijn();
            Assert.AreEqual(voorraad.GetAantalLijnen(), 0, "Voorraad is niet verwijderd");
        }

        [TestMethod]
        public void Test_Opdracht8()
        {
            Sporter sporter = new Sporter(MoveCollection.GetWillekeurigeMoves());

            Assert.IsNotNull(sporter.Moves);
            Assert.IsNotNull(sporter.Skies);
            Assert.IsNotNull(sporter.Zwemvest);

            Assert.IsTrue(sporter.Moves.Count != 0, "Sporter moet wel moves hebben");
            if (sporter.AantalRondes != 1 & sporter.AantalRondes != 2) Assert.Fail("Sporter moet 1 of 2 rondes hebben");

        }

        [TestMethod]
        public void Test_Opdracht10()
        {
            Wachtrij wachtrijInstructie = new WachtrijInstructie();
            Wachtrij wachtrijStarten = new WachtrijStarten();
            Wachtrij instructieGroep = new InstructieGroep();

            Assert.IsTrue(wachtrijInstructie.MAX_LENGTE_WACHTRIJ == 100, "Instructie wachtrij moet lengte 100 hebben");
            Assert.IsTrue(wachtrijStarten.MAX_LENGTE_WACHTRIJ == 20, "Start wachtrij moet lengte 20 hebben");
            Assert.IsTrue(instructieGroep.MAX_LENGTE_WACHTRIJ == 5, "Instructie groep moet lengte 5 hebben");
        }
    }
}
