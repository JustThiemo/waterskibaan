﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waterskibaan
{
    public class Sporter
    {

        private static readonly Random rnd = new Random();

        public Zwemvest Zwemvest { get; set; }
        public Skies Skies { get; set; }
        public Color KledingKleur { get; set; }
        public List<IMoves> Moves { get; set; }
        public IMoves HuidigeMove { get; set; }
        public int AantalPunten { get; set; }
        public int AantalRondes { get; set; }
        public Lijn OpLijn { get; set; }

        public Sporter(List<IMoves> moves)
        {
            Moves = moves;
            AantalPunten = 0;
            AantalRondes = rnd.Next(2) + 1;
            KledingKleur = GetRandomKleur();
        }

        public bool DoMove(IMoves move)
        {
            if (move == null || !Moves.Contains(move)) throw new NotSupportedException("Can not execute move which hasn't been taught! :(");
            if (DidSucceed())
            {
                AantalPunten += move.Move();
                HuidigeMove = move;
                return true;
            }
            return false;
        }

        public bool DoMove()
        {
            IMoves move = Moves[rnd.Next(Moves.Count)];
            if (move != null) if (DidSucceed())
                {
                    AantalPunten += move.Move();
                    HuidigeMove = move;
                    return true;
                }
            return false;

        }

        private bool DidSucceed() => rnd.Next(2) == 1;

        private static Color GetRandomKleur() => Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));

        public bool HeeftVest() => Zwemvest != null;

        public bool HeeftSkies() => Skies != null;

        public override string ToString() => $"Aantal moves: {Moves.Count} Kleur: {KledingKleur.ToArgb()} Punten: {AantalPunten} Lijn: {OpLijn?.PositieOpDeKabel}";
    }
}
