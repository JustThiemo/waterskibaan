﻿using System;
using System.Collections.Generic;
using Waterskibaan.Moves;

namespace Waterskibaan
{
    public static class MoveCollection
    {
        private static List<IMoves> AvailableMoves = new List<IMoves>(
            new IMoves[] {
                new MoveSpin(), new MoveJump(), new Move360(), new MoveEenHand(), new MoveSalto()
            });

        private static readonly Random random = new Random();

        public static List<IMoves> GetWillekeurigeMoves()
        {
            List<IMoves> moves = new List<IMoves>();
            int amount = random.Next(4) + 1;
            for (int i = 0; i < amount; i++) moves.Add(AvailableMoves[random.Next(AvailableMoves.Count)]);
            return moves;
        }
    }
}
