﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waterskibaan.Wachtrij
{
    interface IWachtrij
    {

        void SporterNeemtPlaatsInRij(Sporter sporter);

        List<Sporter> GetAlleSporters();

        List<Sporter> SportersVerlatenRij(int aantal);

    }
}
