﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waterskibaan.Wachtrij
{
    public class InstructieGroep : Wachtrij
    {
        public InstructieGroep() : base(5)
        {

        }

        public void OnInstructieAfgelopen(object sender, InstructieEindeArgs eventArgs)
        {
            foreach (Sporter sporter in eventArgs.Groep)
            {
                eventArgs.Wachtrij.SporterNeemtPlaatsInRij(sporter);
            }
        }

        public override string GetNaam() => "InstructieGroep";
    }
}
