﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waterskibaan.Wachtrij
{
    public abstract class Wachtrij : IWachtrij
    {
        public int MAX_LENGTE_WACHTRIJ { get; }

        public Wachtrij(int wachtRijLengte)
        {
            MAX_LENGTE_WACHTRIJ = wachtRijLengte;
        }

        public Queue<Sporter> Queue { get; } = new Queue<Sporter>();

        public abstract string GetNaam();

        public List<Sporter> GetAlleSporters() => Queue.ToList();

        public void SporterNeemtPlaatsInRij(Sporter sporter)
        {
            if (Queue.Count >= MAX_LENGTE_WACHTRIJ) return;
            Queue.Enqueue(sporter);
        }

        public List<Sporter> SportersVerlatenRij(int aantal)
        {
            if (Queue.Count == 0) return Queue.ToList();

            for (int i = 0; i < aantal; i++) Queue.Dequeue();

            return Queue.ToList();
        }

        public override string ToString() => $"{GetNaam()}: {string.Join(", ", Queue.ToList())}";
    }
}
