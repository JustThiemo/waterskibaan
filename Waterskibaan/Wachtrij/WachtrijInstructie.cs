﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waterskibaan.Wachtrij
{
    public class WachtrijInstructie : Wachtrij
    {
        public WachtrijInstructie() : base(100)
        {
        }

        public override string GetNaam() => "WachtrijInstructie";

        public void OnNieuweBezoeker(object sender, SporterEventArgs eventArgs)
        {
            SporterNeemtPlaatsInRij(eventArgs.Sporter);
        }
    }
}
