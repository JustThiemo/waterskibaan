﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waterskibaan.Wachtrij
{
    public class WachtrijStarten : Wachtrij
    {
        public WachtrijStarten() : base(20)
        {
        }

        public void OnLijnenVerplaatst(object sender, EventArgs args)
        {
            WaterSkibaan waterskibaan = (WaterSkibaan)sender;

            if (Queue.Count == 0 || !waterskibaan._kabel.IsStartPositieLeeg()) return;

            Sporter sporter = Queue.Dequeue();
            sporter.Zwemvest = new Zwemvest();
            sporter.Skies = new Skies();
            waterskibaan.SporterStart(sporter);
        }

        public override string GetNaam() => "WachtrijStarten";
    }
}
