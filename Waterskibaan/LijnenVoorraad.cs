﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waterskibaan
{
    public class LijnenVoorraad
    {

        private Queue<Lijn> _lijnen = new Queue<Lijn>();

        public void LijnToevoegenAanRij(Lijn lijn)
        {
            // Make sure the given line is not null
            if (lijn == null) return;
            _lijnen.Enqueue(lijn);
        }

        public Lijn VerwijderEersteLijn()
        {
            if (_lijnen.Count == 0) return null;
            return _lijnen.Dequeue();
        }

        public Lijn EersteLijn()
        {
            if (_lijnen.Count == 0) return null;
            return _lijnen.Dequeue();
        }

        public int GetAantalLijnen() => _lijnen.Count;

        public override string ToString() => $"{this.GetAantalLijnen()} lijnen op voorraad";


    }
}
