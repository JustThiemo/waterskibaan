﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Waterskibaan.Wachtrij;

namespace Waterskibaan
{

    public class SporterEventArgs : EventArgs
    {
        public Sporter Sporter { get; }

        public SporterEventArgs(Sporter sporter)
        {
            Sporter = sporter;
        }
    }

    public class InstructieEindeArgs : EventArgs
    {

        public WachtrijStarten Wachtrij { get; }
        public Queue<Sporter> Groep { get; }

        public InstructieEindeArgs(WachtrijStarten rij, Queue<Sporter> groepje)
        {
            Wachtrij = rij;
            Groep = groepje;
        }
    }

    public class Game
    {
        public readonly WaterSkibaan skibaan;
        public readonly WachtrijInstructie wachtrijInstructie;
        public readonly InstructieGroep instructieGroep;
        public readonly WachtrijStarten wachtrijStarten;
        public readonly Logger logger;

        public event NieuweBezoekerHandler NieuweBezoeker;
        public event InstructieAfgelopenHandler InstructieAfgelopen;

        public delegate void NieuweBezoekerHandler(object sender, SporterEventArgs args);
        public delegate void InstructieAfgelopenHandler(object sender, InstructieEindeArgs args);

        public int _ticks;

        public Game()
        {
            skibaan = new WaterSkibaan();
            skibaan.game = this;

            instructieGroep = new InstructieGroep();
            wachtrijInstructie = new WachtrijInstructie();
            wachtrijStarten = new WachtrijStarten();
            logger = new Logger(skibaan._kabel);
        }

        public void Initialize()
        {

            NieuweBezoeker += wachtrijInstructie.OnNieuweBezoeker;
            NieuweBezoeker += logger.OnNieuweBezoeker;
            InstructieAfgelopen += instructieGroep.OnInstructieAfgelopen;
            skibaan.LijnenVerplaats += wachtrijStarten.OnLijnenVerplaatst;

        }

        public void OnTimePassed(object sender, ElapsedEventArgs args)
        {
            _ticks++;
            if (_ticks % 1 == 0)
            {
                Console.WriteLine("Nieuwe bezoeker!");
                KomtNieuweBezoeker();
            }

            if (_ticks % 1 == 0 && wachtrijInstructie.Queue.Count >= 5 && instructieGroep.Queue.Count == 0)
            {

                for (int i = 0; i < 5; i++)
                {
                    Sporter sporter = wachtrijInstructie.Queue.Dequeue();
                    if (sporter != null)
                    {
                        instructieGroep.SporterNeemtPlaatsInRij(sporter);
                    }
                }
            }


            if (_ticks % 1 == 0)
            {
                VerplaatstLijntjes();

            }

            if (_ticks % 10 == 0)
            {
                Console.WriteLine("Instructie afgelopen. 5 gaan naar de wachtrij starten");
                InstructieIsAfgelopen();
            }
        }

        private void VerplaatstLijntjes()
        {
            skibaan.VerplaatsKabel();
        }

        private void InstructieIsAfgelopen()
        {
            Queue<Sporter> sporters = new Queue<Sporter>();
            if (instructieGroep.Queue.Count > 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    Sporter first = instructieGroep.Queue.Dequeue();
                    if (first != null)
                    {
                        sporters.Enqueue(first);
                    }
                }
                InstructieAfgelopen.Invoke(this, new InstructieEindeArgs(wachtrijStarten, sporters));
            }
        }

        private void KomtNieuweBezoeker()
        {
            Sporter sporter = new Sporter(MoveCollection.GetWillekeurigeMoves());
            NieuweBezoeker.Invoke(this, new SporterEventArgs(sporter));
        }
    }
}
