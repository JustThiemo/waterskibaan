﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waterskibaan
{
    public class WaterSkibaan
    {

        public Game game { get; set; }
        public readonly LijnenVoorraad _voorraad = new LijnenVoorraad();
        public readonly Kabel _kabel;

        public delegate void LijnenVerplaatsHandler(object sender, EventArgs args);
        public event LijnenVerplaatsHandler LijnenVerplaats;

        public WaterSkibaan()
        {
            _kabel = new Kabel(this);
            for (int i = 0; i < 15; i++) _voorraad.LijnToevoegenAanRij(new Lijn());
        }

        public void SporterStart(Sporter sporter)
        {
            if (!sporter.HeeftSkies() | !sporter.HeeftVest()) throw new NotSupportedException("Missing needed requirements to start.");
            if (!_kabel.IsStartPositieLeeg()) return;

            Lijn newLine = _voorraad.EersteLijn();
            if(newLine == null)
            {
                return;
            }
            newLine.Sporter = sporter;
            _kabel.NeemLijnInGebruik(newLine);
            sporter.OpLijn = newLine;
        }

        public void VerplaatsKabel()
        {
            _kabel.VerschuifLijnen();
            Lijn verwijderMij = _kabel.VerwijderLijnVanKabel();
            if (verwijderMij != null)
            {
                verwijderMij.PositieOpDeKabel = 0;
                verwijderMij.Sporter = null;
                _voorraad.LijnToevoegenAanRij(verwijderMij);
            }
           
            LijnenVerplaats.Invoke(this, EventArgs.Empty);
        }
        public override string ToString() => $"voorraad: {_voorraad.ToString()}, kabel: {_kabel.ToString()}";

    }
}
