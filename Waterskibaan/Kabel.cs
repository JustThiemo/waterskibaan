﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waterskibaan
{
    public class Kabel
    {

        private WaterSkibaan skibaan;
        public Kabel(WaterSkibaan baan)
        {
            skibaan = baan;
        }

        public LinkedList<Lijn> _lijnen = new LinkedList<Lijn>();

        public bool IsStartPositieLeeg()
        {
            foreach (Lijn lijn in _lijnen) if (lijn.PositieOpDeKabel == 0) return false;
            return true;
        }

        public void NeemLijnInGebruik(Lijn lijn)
        {
            if (!IsStartPositieLeeg()) return;
            _lijnen.AddFirst(lijn);
        }

        public void VerschuifLijnen()
        {
            lock (_lijnen)
            {
                if (_lijnen.Count == 0)
                {
                    return;
                }

                foreach (var lijn in _lijnen)
                {
                    lijn.PositieOpDeKabel++;
                }

                if (_lijnen.Last.Value.PositieOpDeKabel == 10)
                {
                    var laatsteLijn = _lijnen.Last;

                    _lijnen.Remove(laatsteLijn);
                    _lijnen.AddFirst(laatsteLijn);

                    laatsteLijn.Value.PositieOpDeKabel = 0;

                    if (laatsteLijn.Value.Sporter != null) {
                        laatsteLijn.Value.Sporter.AantalRondes--;
                    }

                }
            }
        }

        public Lijn VerwijderLijnVanKabel()
        {
            lock (_lijnen)
            {
                if(_lijnen.Count == 0)                
                    return null;
                
                Lijn lijn = _lijnen.Last.Value;

                if (lijn == null || lijn.PositieOpDeKabel != 9)
                    return null;
                if (lijn.Sporter != null && lijn.Sporter.AantalRondes != 1)
                    return null;
                Lijn laatste = _lijnen.Last();
                _lijnen.RemoveLast();
                return laatste;
            }
            return null;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (Lijn lijn in _lijnen) stringBuilder.Append(lijn.PositieOpDeKabel).Append("|");

            return stringBuilder.ToString().Substring(0, stringBuilder.ToString().Length);
        }


    }
}
