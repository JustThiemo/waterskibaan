﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waterskibaan.Moves
{
    public class MoveSalto : IMoves
    {
        string IMoves.Naam => "Salto";
        public int Move()
        {
            return 5;
        }
    }
}
