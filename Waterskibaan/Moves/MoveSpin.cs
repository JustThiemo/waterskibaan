﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waterskibaan.Moves
{
    public class MoveSpin : IMoves
    {
        string IMoves.Naam => "Spin";

        public int Move()
        {
            return 1;
        }
    }
}
