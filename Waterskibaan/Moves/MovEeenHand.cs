﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waterskibaan.Moves
{
    public class MoveEenHand : IMoves
    {
        string IMoves.Naam => "Een Hand";
        public int Move()
        {
            return 5;
        }
    }
}
