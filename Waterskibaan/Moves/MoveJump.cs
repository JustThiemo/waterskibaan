﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waterskibaan.Moves
{
    public class MoveJump : IMoves
    {
        string IMoves.Naam => "Jump";
        public int Move()
        {
            return 3;
        }
    }
}
